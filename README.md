# Theme Starter Kit

This project will create the basic files and folder structure you will need to 
create a theme for Hubzilla.

An example of a theme derived from 'redbasic', the standard Hubzilla theme, can be found
in the "derivedtheme" folder. Eventually there will be another folder that will get you 
started creating a brand new theme.

## Making it your own
* Rename the `derivedtheme` folder to whatever you want your theme to be called. It is 
case-sensitive, and spaces are not allowed (but underscores are).
* Change the function name in `php/theme.php` so that it is your theme name +`_init`.
* In `php/style.php`, change the folder name in the `file_get_contents` call.

These are the only three changes necessary to be able to test your theme on your Hubzilla install.

## Testing changes in development
To test this in your Hubzilla install, copy the newly-renamed derivedtheme folder to 
`/extend/theme` or another subfolder.
(If you want to collect all your own themes in a particular directory, for example, you
could create `extend/theme/yourname/yourthemename` instead.)

You will also need to create a symbolic link to derived theme for testing, so the Hubzilla
install knows that there is a new theme in the `extend/theme` folder. This step is done
automatically when you use the script to add a theme repository, but for development purposes
you will want to do it manually (unless you have a git repository with your theme already
set up, but then you are probably not reading this guide.)

From the Hubzilla folder on the command line on your server, go down into the theme directory:
`cd view/theme`

Now create the symbolic link using: `ln -s ../../extend/theme/yourthemename yourthemename`.

If you log into your Hubzilla instance as the admin, you should now see your theme listed
in the admin panel list of addons. 