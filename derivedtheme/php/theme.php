<?php

/**
 *   * Name: Derived Theme
 *   * Description: If you see this description, you should update the comment block in theme.php!
 *   * Version: 1.0
 *   * MinVersion: 2.3.1
 *   * MaxVersion: 4.0
 *   * Author: Your Name Here
 *   * Compat: Hubzilla [*]
 */

/**
 *  Replace the "derivedtheme" in this function with your theme name. It is case-sensitive.
 */
function derivedtheme_init(&$a) {
    App::$theme_info['extends'] = 'redbasic';
}